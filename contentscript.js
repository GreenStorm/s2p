//initalize:
var allowed = ['www.zappos.com','www.aliexpress.com','www.amazon.com'],
  currentDomain,
  scrapper, // array contains data.
  siteURL,
  isSupplied = false;
siteURL = document.location.href;
currentDomain = document.location.hostname;
pathName = document.location.pathname;
scrapper = new Array();

//main functionality:
if(isSupplier(currentDomain)){
  scrape();
}
else if(isPO() ){
  setSupplyStatus();
}
else{
  alert(" Oops, this page isn't supported yet :( ");
}

// scrape functions:
function scrape () {
    if(currentDomain==='www.zappos.com')
      zapposScrape();
    else if(currentDomain=== 'www.amazon.com')
      amazonScrape();
    else if (currentDomain=== 'www.aliexpress.com')
      aliExpressScrape();

    var jsonScrapper  = JSON.stringify(scrapper);
    chrome.runtime.sendMessage({method:"setScrapper", scrapper:jsonScrapper}); //sends json scrapper to background.js
}

function aliExpressScrape () {
    var name;
    if(document.querySelector(".product-name") !== null)
      name =  document.querySelector(".product-name").innerText;
    if(document.querySelector("#product-name") !== null)
      name =  document.querySelector("#product-name").innerText;
    scrapper[0] = name;//name
    scrapper[1] = 2; // supplier option
    scrapper[2] = xpathEvaluate('/html/body/div[2]/div[1]/div/a[3]'); // cat
    scrapper[3] = document.querySelector("span.brand").parentNode.nextSibling.nextSibling.innerText; //brand
    scrapper[4] = 'z';//model
    scrapper[5] = siteURL;//url
}

function amazonScrape () {
    scrapper[0] = xpathEvaluate('//*[@id="btAsinTitle"]'); // name
    scrapper[1] = 3;// supplier option: amazon.com
    scrapper[2] = trimString(xpathEvaluate('//*[@id="nav-subnav"]/li[1]/a')); // cat
    scrapper[3] = xpathEvaluate('//*[@id="handleBuy"]/div[1]/span/a'); // brand ---
    scrapper[4] = 'z'; //model
    scrapper[5] = siteURL; //url
}

function zapposScrape(){
  //scrapper[0] = document.getElementsByClassName("fn gae-click*Product-Page*Title*ProductName")[0].innerHTML;
    scrapper[0] = xpathEvaluate('//*[@id="productStage"]/h1/a[2]'); // name
    scrapper[1] = 1;// supplier option: zappos
    scrapper[2] =  xpathEvaluate('//*[@id="crumbs"]/a[2]'); // cat
    scrapper[3] =  xpathEvaluate('//*[@id="productStage"]/h1/a[1]');  // brand
    scrapper[4] = 'z'; //model
    scrapper[5] = siteURL;//url
}


//injection to po data:
function injectDataToPO () {
  retrieveScrapper();
  var productAlreadyExist = document.getElementById("products-table").rows.length>1;
  setTimeout(function(){
    if(!productAlreadyExist){
      injectPlacedToPO();
    }
    injectNewProductToPO();
  },100);
}

function injectPlacedToPO () {
  document.getElementsByName("placed[category_id]")[0].value = getCategoryValue();
  document.getElementsByName("placed[name]")[0].value = scrapper [0];  //name
  document.getElementsByName("placed[supplier_id]")[0].value = scrapper[1]; //supplier (e.g. amazon)
  document.getElementsByName("placed[type]")[1].checked = true; //Radio button switched to new products.
}

function injectNewProductToPO () {
  document.getElementsByName("new-product[manufacturer]")[0].value = getBrandValue();
  document.getElementsByName("new-product[model]")[0].value= "z"; // Z is a placeholder used by all the data-entrier.
  document.getElementsByName("new-product[en_name]")[0].value= scrapper[0];
}

function getCategoryValue () {
  if(stringContains("bag",scrapper[2]))
    return 1;
  else if(stringContains("shoe",scrapper[2]))
    return 4;
  else if(stringContains("makeup",scrapper[2]))
    return 5;
  else if(stringContains("kid",scrapper[2]) || stringContains("baby",scrapper[2]) || stringContains("babbies",scrapper[2]))
    return 6;
  else if(stringContains("cloth",scrapper[2]))
    return 7;
  else
    return 0;
}

function getBrandValue () { // Travesrse manufacturer drop down list and returns the value of the brand
  brands = document.getElementsByName("new-product[manufacturer]")[0].options;
    for (var i=0; i<brands.length;i++){ // checks if it's defined "quick evaluation and "  with the brand string trimmed and captilzed in both ends.
         if((typeof brands[i].innerText == "undefined") && trimString(brands[i].innerText).toString().toUpperCase() === scrapper[3].toString().toUpperCase()){
            return brands[i].value;
         }
    }
   return "0"; // return blank if brand doesn't exists.
}


//helper functions:
function retrieveScrapper () { //from background.js
  chrome.runtime.sendMessage({method:'getScrapper'}, function(response){
    scrapper = JSON.parse(response);
  });
}

function isSupplier (supplier) {
   if (allowed.indexOf(supplier)== -1 ){
      return false;
  }
  return true;
}

function xpathEvaluate (xpath) { // uses xpath to get the element
    var element = document.evaluate(xpath ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;
    if (element !== null)
      return element.innerHTML;
    else
      return null;
}

function trimString (str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function stringContains (partOf,whole) {
  var regExp = new RegExp (partOf,"gi"); // Do a global, case-insensitive search for whole in partOf
  return regExp.test(whole);
}

function containsCategory(partOf) {
  return stringContains(partOf,scrapper[2]);
}

function setSupplyStatus () {
  chrome.runtime.sendMessage({method:'getStatus'}, function(response){
      isSupplied = response.status;
      poLogic(); // calling the injection logic here, because the chrome call is asynchronous. 
  });
}

function poLogic () {
    if(isSupplied === true) // isSupplied not isSupplier
      injectDataToPO();
    else{
      alert("Please select a prodcut first ! ");
    }
}


function isPO () {
  if (pathName.substring(1,3) === "po")
    return true;
  else
    return false;
}